
SCRIPTS = dvbdev persistent-storage ptpdev usbdev
CONF = mdev.conf

libexecdir = /lib/mdev
sysconfdir = /etc

.PHONY: all
all:

tests/Kyuafile: $(wildcard tests/*_test)
	echo "syntax(2)" > $@.tmp
	echo 'test_suite("mdev-conf")' >> $@.tmp
	for i in $(notdir $(wildcard tests/*_test)); do \
		echo "atf_test_program{name='$$i',timeout=1}" >> $@.tmp ; \
	done
	mv $@.tmp $@

Kyuafile:
	echo "syntax(2)" > $@.tmp
	echo "test_suite('mdev-conf')" >> $@.tmp
	echo "include('tests/Kyuafile')" >> $@.tmp
	mv $@.tmp $@

.PHONY: check
check: tests/Kyuafile Kyuafile
	kyua test || { kyua report --verbose && exit 1; }

.PHONY: clean
clean:
	rm -f tests/Kyuafile Kyuafile

.PHONY: install
install: $(SCRIPTS) $(CONF)
	install -d $(DESTDIR)$(libexecdir) $(DESTDIR)$(sysconfdir)
	install -m755 -t $(DESTDIR)$(libexecdir) $(SCRIPTS)
	install -m644 -t $(DESTDIR)$(sysconfdir) $(CONF)
